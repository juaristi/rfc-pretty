#!/usr/bin/env python3
import sys
from rfc_pretty.datatracker import DatatrackerRfcReader

def run():
    if len(sys.argv) < 3:
        print("Usage: %s <action> <RFC number>" % sys.argv[0])
        return 1

    reader = DatatrackerRfcReader()

    rfc_number = int(sys.argv[2])
    action = sys.argv[1]
    if action not in ['-index', '-abstract', '-title']:
        print("Unknown action: '%s'" % action)
        return 1

    rfc = reader.read(rfc_number)
    if action == '-index':
        for sect in rfc.sections:
            print(sect)
    elif action == '-abstract':
        print(rfc.abstract)
    elif action == '-title':
        print(rfc.title)

    return 0

if __name__ == '__main__':
    sys.exit(run())

