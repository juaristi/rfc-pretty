from rfc_pretty.datatracker import DatatrackerRfcReader

class _Iterator(object):
    def __init__(self, sections):
        self._num = 0
        self._sections = sections

    def __iter__(self):
        return self

    def __next__(self):
        if self._num == len(self._sections):
            raise StopIteration()
        sect = self._sections[self._num]
        self._num = self._num + 1
        return sect.tostr()

class RfcReader(object):
    def __init__(self):
        self._parser = None

    def read(self, rfc_number):
        self._parser = DatatrackerRfcReader().read(rfc_number)

    def get_index(self, depth=0):
        if self._parser is None:
            raise RuntimeError("Did you forget to call read?")
        if depth not in [1, 2]:
            raise RuntimeError("Unsupported depth")

        return _Iterator(self._parser.sections)

