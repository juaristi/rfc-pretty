import os
import pytest
from rfc_pretty.datatracker import DatatrackerRfcReader

def test_parser_real():
    directory = os.path.abspath(os.path.dirname(__file__))
    html_file = os.path.join(directory, 'test-full.html')

    r = DatatrackerRfcReader()

    with open(html_file) as fp:
        parser = r.read(fp)

    assert parser.title == 'Hypertext Transfer Protocol -- HTTP/1.1'

    sect1 = parser.sections[0]
    assert sect1.title == 'Notational Conventions and Generic Grammar'
    assert sect1.section_number == '2'
    assert sect1.has_children == True
    assert sect1.children[0].title == 'Augmented BNF'
    assert sect1.children[0].section_number == '2.1'
    assert sect1.children[1].title == 'Basic Rules'
    assert sect1.children[1].section_number == '2.2'

    sect2 = parser.sections[1]
    assert sect2.title == 'Protocol Parameters'
    assert sect2.section_number == '3'

    assert sect2.children[0].title == 'HTTP Version'
    assert sect2.children[0].section_number == '3.1'
    assert sect2.children[0].has_children == False

    assert sect2.children[1].title == 'Uniform Resource Identifiers'
    assert sect2.children[1].section_number == '3.2'
    assert sect2.children[1].has_children == True

    assert sect2.children[1].children[0].title == 'General Syntax'
    assert sect2.children[1].children[0].section_number == '3.2.1'
    assert sect2.children[1].children[1].title == 'http URL'
    assert sect2.children[1].children[1].section_number == '3.2.2'
    assert sect2.children[1].children[2].title == 'URI Comparison'
    assert sect2.children[1].children[2].section_number == '3.2.3'

    sect3 = parser.sections[2]
    assert sect3.title == 'Caching in HTTP'
    assert sect3.section_number == '13'

    assert sect3.children[0].title is None
    assert sect3.children[0].section_number is None
    assert sect3.children[0].children[0].title == 'Cache Correctness'
    assert sect3.children[0].children[1].title == 'Warnings'
    assert sect3.children[0].children[2].title == 'Cache-control Mechanisms'

    assert sect3.children[1].title == 'Expiration Model'
    assert sect3.children[1].section_number == '13.2'
    assert sect3.children[1].children[0].title == 'Server-Specified Expiration'
    assert sect3.children[1].children[0].section_number == '13.2.1'
    assert sect3.children[1].children[1].title == 'Heuristic Expiration'
    assert sect3.children[1].children[1].section_number == '13.2.2'
    assert sect3.children[1].children[2].title == 'Age Calculations'
    assert sect3.children[1].children[2].section_number == '13.2.3'

    sect4 = parser.sections[3]
    assert sect4.title == 'Index'
    assert sect4.section_number == '20'
    assert sect4.has_children == False

    sect5 = parser.sections[4]
    assert sect5.title == '.  Full Copyright Statement'
    assert sect5.section_number == '21'
    assert sect5.has_children == False

