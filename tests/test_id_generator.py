from rfc_pretty import HtmlIdGenerator

def test_id_generator():
    idgen = HtmlIdGenerator(track_duplicates=False)
    assert idgen.id("Introduction") == "introduction"
    assert idgen.id("introduction") == "introduction"
    assert idgen.id(" Introduction ") == "introduction"
    assert idgen.id(" introduction ") == "introduction"
    assert idgen.id("  purpose") == "purpose"
    assert idgen.id("purpose  ") == "purpose"
    assert idgen.id("  PurpoSe") == "purpose"
    assert idgen.id("PuRpoSe  ") == "purpose"

def test_id_generator_with_nonword_chars():
    idgen = HtmlIdGenerator(track_duplicates=False)
    assert idgen.id(" . introduction") == "introduction"
    assert idgen.id(" . Introduction") == "introduction"
    assert idgen.id(". introduction") == "introduction"
    assert idgen.id(". Introduction") == "introduction"
    assert idgen.id(" .introduction") == "introduction"
    assert idgen.id(" .Introduction") == "introduction"

def test_id_generator_multiple_words():
    idgen = HtmlIdGenerator(track_duplicates=False)
    assert idgen.id("Requirements Language") == "requirements-language"
    assert idgen.id("rEquirements Language") == "requirements-language"
    assert idgen.id("requirements Language") == "requirements-language"
    assert idgen.id("Requirements lAnguage") == "requirements-language"
    assert idgen.id("Protocol overview") == "protocol-overview"
    assert idgen.id("protocol overview") == "protocol-overview"
    assert idgen.id("Semantics of thisUpdate, nextUpdate, and producedAt") == \
        "semantics-of-thisupdate-nextupdate-and-producedat"
    assert idgen.id("Response Pre-Production") == "response-pre-production"

def test_id_generator_tracking_duplicates():
    idgen = HtmlIdGenerator()
    assert idgen.id("Introduction") == "introduction"
    assert idgen.id("introduction") == "introduction-2"
    assert idgen.id(" Introduction ") == "introduction-3"
    assert idgen.id(" introduction ") == "introduction-4"
    assert idgen.id(" . introduction") == "introduction-5"
    assert idgen.id(" . Introduction") == "introduction-6"
    assert idgen.id(". introduction") == "introduction-7"
    assert idgen.id(". Introduction") == "introduction-8"
    assert idgen.id(" .introduction") == "introduction-9"
    assert idgen.id(" .Introduction") == "introduction-10"
    assert idgen.id("Requirements Language") == "requirements-language"
    assert idgen.id("rEquirements Language") == "requirements-language-2"
    assert idgen.id("requirements Language") == "requirements-language-3"
    assert idgen.id("Requirements lAnguage") == "requirements-language-4"
    assert idgen.id("Protocol overview") == "protocol-overview"
    assert idgen.id("protocol overview") == "protocol-overview-2"
    assert idgen.id("Semantics of thisUpdate, nextUpdate, and producedAt") == \
        "semantics-of-thisupdate-nextupdate-and-producedat"
    assert idgen.id("Semantics of thisUpdate, nextUpdate, And ProducedAt") == \
        "semantics-of-thisupdate-nextupdate-and-producedat-2"
    assert idgen.id("Response Pre-Production") == "response-pre-production"
    assert idgen.id("Response Pre-pRoduction") == "response-pre-production-2"
    assert idgen.id("Response pre-production") == "response-pre-production-3"
    assert idgen.id("response pre-Production") == "response-pre-production-4"
    assert idgen.id("Response Pre-Production") == "response-pre-production-5"

def test_id_generator_special_chars():
    idgen = HtmlIdGenerator()
    assert len(idgen.id("%/(=·),;:")) == 10
    result = idgen.id("         ")
    assert len(result) == 10 and result != "         "
    assert idgen.id(" - - - -") == "-------"
    assert idgen.id(" - - - -") == "--------2"
    assert idgen.id("- - - - ") == "--------3"
    assert idgen.id("- - - -") == "--------4"
