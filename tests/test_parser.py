import pytest
from io import StringIO
from rfc_pretty.datatracker import DatatrackerRfcReader

@pytest.fixture
def rfc():
    html = """
    <html>
    <head><title>Test</title></head>
    <body>
      <pre>
        <span class="h1"><h1>This is the title</h1></span>
        Abstract

          This is the abstract.

        Status of This Memo

          Blah.

        <span class="h2"><a href="#section-1">1.</a> Introduction</span>
        <span class="h2"><a href="#section-2">2.</a> Notational Conventions and Generic Grammar</span>
        <span class="h3"><a href="#section-2.1">2.1.</a> Augmented BNF</span>
        <span class="h3"><a href="#section-2.2">2.2.</a> Basic Rules</span>
        <span class="h2"><a href="#section-3">3.</a> Protocol Parameters</span>
        <span class="h3"><a href="#section-3.1">3.1.</a> HTTP Version</span>
        <span class="h3"><a href="#section-3.2">3.2.</a> Uniform Resource Identifiers</span>
        <span class="h4"><a href="#section-3.2.1">3.2.1.</a> General Syntax</span>
        <span class="h4"><a href="#section-3.2.2">3.2.2.</a> http URL</span>
        <span class="h3"><a href="#section-3.3">3.3.</a> Date/Time Formats</span>
        <span class="h4"><a href="#section-3.3.1">3.3.1.</a> Full Date</span>
        <span class="h4"><a href="#section-3.3.2">3.3.2.</a> Delta Seconds</span>
        <span class="h3"><a href="#section-3.4">3.4.</a> Character Sets</span>
        <span class="h2"><a href="#section-4">4.</a> HTTP Message</span>
        <span class="h2"><a href="#section-5">5.</a> 5th section</span>
        <span class="h3"><a href="#section-5.1">5.1.</a> Entity Body</span>
        <span class="h4"><a href="#section-5.1.1">5.1.1.</a> Type</span>
        <span class="h4"><a href="#section-5.1.2">5.1.2.</a> Entity Length</span>
        <span class="h2"><a href="#section-6">6.</a> 6th section</span>
        <span class="h2"><a href="#section-7">7.</a> 7th section</span>
        <span class="h4"><a href="#section-7.1.1">7.1.1.</a> Blah</span>
        <span class="h4"><a href="#section-7.1.2">7.1.2.</a> Foo</span>
        <span class="h2"><a href="#section-8">8.</a> 8th section</span>
        <span class="h2"><a href="#section-9">9.</a> 9th section</span>
        <span class="h5"><a href="#section-9.1.1.1">9.1.1.1.</a> Foo</span>
        <span class="h6"><a href="#section-9.1.1.1.1">9.1.1.1.1.</a> Bar</span>
        <span class="h5"><a href="#section-9.1.1.2">9.1.1.2.</a> Baz</span>
        <span class="h2"><a href="#section-10">10.</a> 10th section</span>
        <span class="h6"><a href="#section-10.1.1.1.1">10.1.1.1.1.</a> Foo</span>
        <span class="h6"><a href="#section-10.1.1.1.2">10.1.1.1.2.</a> Bar</span>
      </pre>
    </body>
    </html>
    """

    reader = DatatrackerRfcReader()
    rfc = reader.read(StringIO(html))

    return rfc

def test_titles(rfc):
    assert rfc.title == 'This is the title'
    assert rfc[0].section_number == '1.'
    assert rfc[0].title == 'Introduction'
    assert rfc[1].section_number == '2.'
    assert rfc[1].title == 'Notational Conventions and Generic Grammar'
    assert rfc[2].section_number == '3.'
    assert rfc[2].title == 'Protocol Parameters'
    assert rfc[3].section_number == '4.'
    assert rfc[3].title == 'HTTP Message'
    assert rfc[4].section_number == '5.'
    assert rfc[4].title == '5th section'
    assert rfc[5].section_number == '6.'
    assert rfc[5].title == '6th section'
    assert rfc[6].section_number == '7.'
    assert rfc[6].title == '7th section'
    assert rfc[7].section_number == '8.'
    assert rfc[7].title == '8th section'
    assert rfc[8].section_number == '9.'
    assert rfc[8].title == '9th section'
    assert rfc[9].section_number == '10.'
    assert rfc[9].title == '10th section'

def test_multi_level_titles(rfc):
    assert rfc[0].has_children == False
    assert len(rfc[0].children) == 0

    sect = rfc[1]
    assert sect.has_children == True
    assert len(sect.children) == 2
    assert sect.children[0].title == 'Augmented BNF'
    assert sect.children[0].section_number == '2.1.'
    assert sect.children[1].title == 'Basic Rules'
    assert sect.children[1].section_number == '2.2.'

    sect = rfc[2]
    assert sect.has_children == True
    assert len(sect.children) == 4
    assert sect.children[0].title == 'HTTP Version'
    assert sect.children[0].section_number == '3.1.'
    assert sect.children[0].has_children == False
    assert len(sect.children[0].children) == 0
    assert sect.children[1].title == 'Uniform Resource Identifiers'
    assert sect.children[1].section_number == '3.2.'
    assert sect.children[1].has_children == True
    assert len(sect.children[1].children) == 2
    assert sect.children[1].children[0].title == 'General Syntax'
    assert sect.children[1].children[0].section_number == '3.2.1.'
    assert sect.children[1].children[0].has_children == False
    assert sect.children[1].children[1].title == 'http URL'
    assert sect.children[1].children[1].section_number == '3.2.2.'
    assert sect.children[1].children[1].has_children == False
    assert sect.children[2].title == 'Date/Time Formats'
    assert sect.children[2].section_number == '3.3.'
    assert sect.children[2].has_children == True
    assert len(sect.children[2].children) == 2
    assert sect.children[2].children[0].title == 'Full Date'
    assert sect.children[2].children[0].section_number == '3.3.1.'
    assert sect.children[2].children[1].title == 'Delta Seconds'
    assert sect.children[2].children[1].section_number == '3.3.2.'
    assert sect.children[3].title == 'Character Sets'
    assert sect.children[3].section_number == '3.4.'
    assert sect.children[3].has_children == False
    assert len(sect.children[3].children) == 0

    assert rfc[3].has_children == False
    assert len(rfc[3].children) == 0

    sect = rfc[4]
    assert sect.has_children == True
    assert len(sect.children) == 1
    assert sect.children[0].title == 'Entity Body'
    assert sect.children[0].section_number == '5.1.'
    assert sect.children[0].has_children == True
    assert len(sect.children[0].children) == 2
    assert sect.children[0].children[0].title == 'Type'
    assert sect.children[0].children[0].section_number == '5.1.1.'
    assert sect.children[0].children[1].title == 'Entity Length'
    assert sect.children[0].children[1].section_number == '5.1.2.'

    assert rfc[5].has_children == False
    assert len(rfc[5].children) == 0

    sect = rfc[6]
    assert sect.has_children == True
    assert len(sect.children) == 1
    assert sect.children[0].has_children == True
    assert len(sect.children[0].children) == 2
    assert sect.children[0].title is None
    assert sect.children[0].section_number is None
    assert sect.children[0].children[0].title == 'Blah'
    assert sect.children[0].children[0].section_number == '7.1.1.'
    assert sect.children[0].children[1].title == 'Foo'
    assert sect.children[0].children[1].section_number == '7.1.2.'


    assert rfc[7].has_children == False
    assert len(rfc[7].children) == 0

    sect = rfc[8]
    assert sect.has_children == True
    assert len(sect.children) == 1
    assert sect.children[0].title is None
    assert sect.children[0].children[0].title is None
    assert len(sect.children[0].children[0].children) == 2
    assert sect.children[0].children[0].children[0].title == 'Foo'
    assert sect.children[0].children[0].children[0].section_number == '9.1.1.1.'
    assert sect.children[0].children[0].children[1].title == 'Baz'
    assert sect.children[0].children[0].children[1].section_number == '9.1.1.2.'
    assert sect.children[0].children[0].children[1].has_children == False
    assert sect.children[0].children[0].children[0].has_children == True
    assert len(sect.children[0].children[0].children[0].children) == 1
    assert sect.children[0].children[0].children[0].children[0].title == 'Bar'
    assert sect.children[0].children[0].children[0].children[0].section_number == '9.1.1.1.1.'

    sect = rfc[9]
    assert sect.has_children == True
    assert len(sect.children) == 1
    assert sect.children[0].has_children == True
    assert len(sect.children[0].children) == 1
    assert sect.children[0].children[0].has_children == True
    assert len(sect.children[0].children[0].children) == 1
    assert sect.children[0].children[0].children[0].has_children == True
    assert len(sect.children[0].children[0].children[0].children) == 2
    assert sect.children[0].children[0].children[0].children[0].has_children == False
    assert sect.children[0].children[0].children[0].children[0].title == 'Foo'
    assert sect.children[0].children[0].children[0].children[0].section_number == '10.1.1.1.1.'
    assert sect.children[0].children[0].children[0].children[1].title == 'Bar'
    assert sect.children[0].children[0].children[0].children[1].section_number == '10.1.1.1.2.'

