ServerRoot "/usr/local"
ServerName "${SERVER_NAME}"
ServerTokens Prod

LogLevel "${LOG_LEVEL}"

LoadModule log_config_module    modules/mod_log_config.so
LoadModule mime_module          modules/mod_mime.so
LoadModule authz_core_module    modules/mod_authz_core.so
LoadModule unixd_module         modules/mod_unixd.so

LoadModule cache_module         modules/mod_cache.so
LoadModule cache_disk_module    modules/mod_cache_disk.so
LoadModule proxy_module         modules/mod_proxy.so
LoadModule proxy_http_module    modules/mod_proxy_http.so
LoadModule headers_module       modules/mod_headers.so

TypesConfig conf/mime.types
PidFile "/var/run/httpd.pid"

# Comment these out if running httpd as a non root user
User httpd
Group httpd

Listen *:80

ErrorLog "|/usr/local/bin/rotatelogs /var/local/httpd/logs/%m-%d-error_log 86400"
LogFormat "%h %t \"%r\" %>s %b" common
CustomLog "|/usr/local/bin/rotatelogs /var/local/httpd/logs/%m-%d-access_log 86400" common

# Disable forward proxying
# Should be disabled by default, but doesn't harm to declare it explicitly
ProxyRequests Off

# Use the sendfile syscall to serve static files
#  - this should include cache hits as well
EnableSendfile On

# Never change this block
# Deny everything by default
<Directory />
    AllowOverride None
    Require all denied
</Directory>

CacheRoot "/var/local/httpd/cache"
CacheDirLevels 3
CacheDirLength 5
CacheQuickHandler Off
# Prevent these headers from being written to the cache
# otherwise, they would be served verbatim on subsequent
# cache hits
CacheIgnoreHeaders Date Server

CacheEnable disk "/"
CacheHeader On
<If "%{osenv:LOG_LEVEL} == 'debug'">
    CacheDetailHeader On
</If>
# Remove the origin server's Server header, and use Apache's own instead
# Apache will always set its Server header if one has not been set before
Header unset Server

<Location />
    Require all granted
    ProxyPass "${BACKEND_SERVER}/"
    ProxyPass "${BACKEND_SERVER}/"
</Location>
