import re
import string
from random import choices
from threading import Lock

RANDOM_STRING_LENGTH = 10

def generate_random_string(length):
    """ Generate a random string of the given length """
    return ''.join(choices(string.ascii_letters + string.digits, k=length))

class HtmlIdGenerator:
    def __init__(self, track_duplicates=True):
        self._re = re.compile(r'[^A-Za-z0-9_\- ]')
        self._generated_ids = set() if track_duplicates else None

    def _gen_id_simple(self, txt):
        return \
        self._re.sub('', txt.strip().lower()) \
            .strip() \
            .replace(" ", "-")

    def _find_unused_id(self, id_prefix, start):
        new_id = f"{id_prefix}-{start}"
        if new_id not in self._generated_ids:
            return new_id
        return self._find_unused_id(id_prefix, start + 1)

    def id(self, txt):
        result = self._gen_id_simple(txt)
        if result == "":
            result = generate_random_string(RANDOM_STRING_LENGTH)
        if self._generated_ids is None:
            return result

        lock = Lock()
        try:
            lock.acquire()
            if result not in self._generated_ids:
                self._generated_ids.add(result)
                return result
            new_result = self._find_unused_id(result, 2)
            self._generated_ids.add(new_result)
            return new_result
        finally:
            lock.release()
