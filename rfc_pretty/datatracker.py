import re
import requests
from io import IOBase, StringIO
from html.parser import HTMLParser
from . import HtmlIdGenerator

class RfcNotFoundError(Exception):
    pass

def clean_title(txt):
    m = re.match("([^a-zA-Z0-9\-_])*(.*)", txt)
    result = m.groups()[-1] if m else txt
    return result.strip()

def split_into_paragraphs(txt):
    parts = txt.encode().split(b'\n\n')
    return [p for p in map(
            lambda e: e.decode().strip(), parts)]

class Section(object):
    def __init__(self):
        self.id = None
        self.section = None
        self.title_parts = []
        self._text_parts = []

    @property
    def section_number(self):
        if len(self.title_parts) == 2:
            return self.title_parts[0]

    @property
    def title(self):
        if self.title_parts:
            return clean_title(self.title_parts[1]) if len(self.title_parts) >= 2 \
                else clean_title(self.title_parts[0])

    @property
    def text(self):
        if self._text_parts:
            return ''.join(self._text_parts)
            # text = ''.join(map(lambda e: e.strip('\n'), self._text_parts))
            # return text

    @property
    def text_paragraphs(self):
        return split_into_paragraphs(self.text)

    @property
    def href(self):
        if self.id is None:
            raise RuntimeError("ID not set for section")
        return f"#{self.id}"

    @property
    def depth(self):
        if isinstance(self.section, tuple):
            return len(self.section)
        return 1

    def append_title(self, text):
        self.title_parts.append(text)

    def append_text(self, text):
        if text.strip():
            if text.startswith('\n') and text.endswith('\n'):
                self._text_parts.append('\n' + text.strip('\n') + '\n')
            elif text.startswith('\n'):
                self._text_parts.append('\n' + text.strip('\n'))
            elif text.endswith('\n'):
                self._text_parts.append(text.strip('\n') + '\n')
            else:
                self._text_parts.append(text)

    def close(self, id_generator):
        self.id = id_generator.id(self.title)
        # if len(self.title_parts) < 2:
        #     raise ValueError("Title section is missing - %s" % str(self.title_parts))
        try:
            self.section = tuple( \
                map(lambda e: int(e), \
                    filter(None, self.title_parts[0].split("."))))
        except ValueError:
            self.section = ''.join(self.title_parts)

    def __str__(self):
        num_spaces = (len(self.section) - 1) if isinstance(self.section, tuple) \
            else 1
        return (" " * num_spaces) + f"{self.section_number} {self.title}"

class FlatIndex(object):
    def __init__(self):
        self.current_section = None
        self._sections = []
        self._id_generator = HtmlIdGenerator()

    def add_section(self):
        self.current_section = Section()
        self._sections.append(self.current_section)

    def end_section(self):
        if self.current_section:
            self.current_section.close(self._id_generator)
            # self.current_section = None

    def append_title(self, text):
        self.current_section.append_title(text)

    def append_text(self, text):
        if self.current_section:
            self.current_section.append_text(text)

    @property
    def sections(self):
        return self._sections

class StackIndex(object):
    def __init__(self):
        self._root = []
        self._current = None
        self._active = None
        self._level = 0

    @property
    def sections(self):
        return self._root

    def add_section(self, level):
        sect = StackedSection()
        if self._current and self._level > 0:
            self._current.append(sect)
        else:
            self._current = sect
            self._root.append(sect)
        self._active = sect

    def push_section(self, level):
        sect = StackedSection()
        self._active.append(sect)

        if level > (self._level + 1):
            sect = self._append_multiple_sections(sect, level)

        self._current = sect.parent
        self._active = sect

        self._level = level

    def pop_section(self, level):
        if self._current.parent and level > 0:
            self._current = self._current.parent
            self._active = self._current
        else:
            self._current = None
            self._active = self._root[-1]
            self._level = level

    def append_title(self, data):
        self._active.append_title(data.strip())

    def append_text(self, data):
        if self._active:
            self._active.append_text(data)

    def _append_multiple_sections(self, sect, level):
        num_additional_levels = level - (self._level + 1)
        return self._append_sections_recursively(sect, num_additional_levels)

    def _append_sections_recursively(self, sect, counter):
        new_sect = StackedSection()
        sect.append(new_sect)

        counter -= 1
        if counter > 0:
            return self._append_sections_recursively(new_sect, counter)

        return new_sect

class StackedSection(Section):
    def __init__(self):
        super().__init__()
        self.parent = None
        self._children = []
        self.expose_children = True

    @property
    def children(self):
        if self.expose_children:
            return self._children
        return []

    @property
    def has_children(self):
        return self.expose_children and len(self._children) > 0

    @property
    def section_number(self):
        if len(self.title_parts) > 0:
            return self.title_parts[0]

    @property
    def href(self):
        # TODO implement this
        return "#introduction"

    def append(self, section):
        section.parent = self
        self._children.append(section)

    def tostr(self):
        data = list(map(lambda e: e.strip(), self.title_parts))
        return ' '.join(data)

    def _print_children(self, spaces):
        strs = []
        for child in self._children:
            strs.append((" " * spaces) + child.tostr())
            strs.extend(child._print_children(spaces + 1))
        return strs

    def print(self):
        data = list(map(lambda e: e.strip(), self.title_parts))
        print(' '.join(data))
        self._print_children(1)

    def __str__(self):
        data = list(map(lambda e: e.strip(), self.title_parts))
        children = self._print_children(1)
        return ' '.join(data) + (("\n" + "\n".join(children)) if len(children) > 0 else '')

class DatatrackerHtmlParserFlat(HTMLParser):
    def __init__(self):
        super().__init__()
        self.title = None
        self._index = FlatIndex()
        self._current_section = 0
        self._abstract_io = StringIO()
        self._reading_title = False
        self._title_read = False
        self._in_section = False
        self._in_abstract = False
        self._abstract_read = False
        self._in_text = False
        self._splitters_passed = 0
        self._in_splitter = False

    @property
    def sections(self):
        return self._index.sections

    @property
    def abstract(self):
        if self._abstract_io.tell() == 0:
            return None
        return self._abstract_io.getvalue().strip()

    def __getitem__(self, key):
        if type(key) != int:
            raise TypeError("Key must be an integer")
        return self.sections[key]

    def handle_starttag(self, tag, attrs):
        attr = self._get_attr(attrs, 'class')
        if tag == 'span' and attr is not None:
            if attr == 'h1' and self._title_read == False:
                self._reading_title = True
            elif attr in ['h2', 'h3', 'h4', 'h5', 'h6']:
                self._in_text = False
                self._splitters_passed = 0
                sect = self._sect2number(attr)
                self._index.add_section()
                self._in_section = True
            elif attr == 'grey':
                if self._in_abstract:
                    self._in_abstract = False
                    self._abstract_read = True
                elif self._in_text:
                    self._splitters_passed += 1
                    #self._index.append_text("Starting splitter\n")
                    self._in_splitter = True
            else:
                return # TODO in the future, raise exception here (unknown length)
        elif tag == 'hr' and attr == 'noprint':
            self._splitters_passed += 1

    def handle_endtag(self, tag):
        if self._reading_title and tag == 'span':
            self._reading_title = False
            self._title_read = True
            return

        if tag == 'span':
            self._in_section = False
            self._index.end_section()
            self._in_text = True
            if self._splitters_passed > 2:
                self._splitters_passed = 0
                #self._index.append_text("Ending splitter")
                self._in_splitter = False

    def handle_data(self, data):
        if self._reading_title:
            self.title = data.strip()
            return

        if self._in_section:
            self._index.append_title(data)
            return

        if not self._abstract_read:
            r = re.search("Abstract", data)
            if r:
                self._in_abstract = True
                self._abstract_read = True

                start, end = r.end(), 0
                r2 = re.search("Status of This Memo", data)
                if r2:
                    end = r2.start()

                self._append_abstract(data, start, end)

                if r2:
                    self._in_abstract = False
            return

        if self._in_abstract:
            if re.search("\[Page [0-9]+\]$", data):
                self._in_abstract = False
                return

            r = re.search("Status of This Memo", data.strip())
            if r:
                self._append_abstract(data, start=r.start())
                self._in_abstract = False
                return

            self._append_abstract(data)
            return

        if self._in_text and not self._in_splitter:
            self._index.append_text(data)
            return

        if self._in_text and self._splitters_passed == 0:
            self._index.append_text(data)
            return

    def _append_abstract(self, text, start=0, end=0):
        if start > 0 and end > start:
            t = text[start:end]
        elif start > 0:
            t = text[start:]
        else:
            t = text
        self._abstract_io.write(t)

    def _get_attr(self, attrs, attr):
        for tup in attrs:
            if tup[0] == attr:
                return tup[1]

    def _sect2number(self, sect):
        if len(sect) == 2 and sect[0] == 'h':
            return int(sect[1]) - 2

class DatatrackerHtmlParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.title = None
        self._index = StackIndex()
        self._current_section = 0
        self._abstract_io = StringIO()
        self._reading_title = False
        self._title_read = False
        self._in_section = False
        self._in_abstract = False
        self._abstract_read = False
        self._in_text = False
        self._splitters_passed = 0
        self._in_splitter = False

    @property
    def sections(self):
        return self._index.sections

    @property
    def abstract(self):
        if self._abstract_io.tell() == 0:
            return None
        return self._abstract_io.getvalue().strip()

    def __getitem__(self, key):
        if type(key) != int:
            raise TypeError("Key must be an integer")
        return self.sections[key]

    def handle_starttag(self, tag, attrs):
        attr = self._get_attr(attrs, 'class')
        if tag == 'span' and attr is not None:
            if attr == 'h1' and self._title_read == False:
                self._reading_title = True
            elif attr in ['h2', 'h3', 'h4', 'h5', 'h6']:
                self._in_text = False
                self._splitters_passed = 0
                sect = self._sect2number(attr)
                if sect == self._current_section:
                    self._in_section = True
                    self._index.add_section(sect)
                elif sect > self._current_section:
                    self._in_section = True
                    self._index.push_section(sect)
                    self._current_section = sect
                elif sect < self._current_section:
                    self._index.pop_section(sect)
                    self._index.add_section(sect)
                    self._in_section = True
                    self._current_section = sect
            elif attr == 'grey':
                if self._in_abstract:
                    self._in_abstract = False
                    self._abstract_read = True
                elif self._in_text:
                    self._splitters_passed += 1
                    #self._index.append_text("Starting splitter\n")
                    self._in_splitter = True
            else:
                return # TODO in the future, raise exception here (unknown length)
        elif tag == 'hr' and attr == 'noprint':
            self._splitters_passed += 1

    def handle_endtag(self, tag):
        if self._reading_title and tag == 'span':
            self._reading_title = False
            self._title_read = True
            return

        if tag == 'span':
            self._in_section = False
            self._in_text = True
            if self._splitters_passed > 2:
                self._splitters_passed = 0
                #self._index.append_text("Ending splitter")
                self._in_splitter = False

    def handle_data(self, data):
        if self._reading_title:
            self.title = data.strip()
            return

        if self._in_section:
            self._index.append_title(data)
            return

        if not self._abstract_read:
            r = re.search("Abstract", data)
            if r:
                self._in_abstract = True
                self._abstract_read = True

                start, end = r.end(), 0
                r2 = re.search("Status of This Memo", data)
                if r2:
                    end = r2.start()

                self._append_abstract(data, start, end)

                if r2:
                    self._in_abstract = False
            return

        if self._in_abstract:
            if re.search("\[Page [0-9]+\]$", data):
                self._in_abstract = False
                return

            r = re.search("Status of This Memo", data.strip())
            if r:
                self._append_abstract(data, start=r.start())
                self._in_abstract = False
                return

            self._append_abstract(data)
            return

        if self._in_text and not self._in_splitter:
            self._index.append_text(data)
            return

    def _append_abstract(self, text, start=0, end=0):
        if start > 0 and end > start:
            t = text[start:end]
        elif start > 0:
            t = text[start:]
        else:
            t = text
        self._abstract_io.write(t)

    def _get_attr(self, attrs, attr):
        for tup in attrs:
            if tup[0] == attr:
                return tup[1]

    def _sect2number(self, sect):
        if len(sect) == 2 and sect[0] == 'h':
            return int(sect[1]) - 2

def rfc_exists(rfc_number):
    url = 'https://datatracker.ietf.org/doc/rfc{}/'.format(rfc_number)
    resp = requests.head(url)
    return resp.status_code != 404

class DatatrackerRfcReader(object):
    def read(self, rfc):
        if type(rfc) == int:
            return self._read_from_datatracker(rfc)
        elif isinstance(rfc, IOBase):
            return self._read_from_io(rfc)
        raise TypeError("Unsupported type")

    def _read_from_io(self, io):
        return self._parse(io.read())

    def _read_from_datatracker(self, rfc_number):
        if not rfc_exists(rfc_number):
            raise RfcNotFoundError()

        url = 'https://www.rfc-editor.org/rfc/rfc{}.html'.format(rfc_number)
        resp = requests.get(url)

        if resp.status_code != 200:
            raise ConnectionError("Server returned HTTP code: {}".format(resp.status_code))

        return self._parse(resp.text)

    def _parse(self, html):
        # parser = DatatrackerHtmlParser()
        parser = DatatrackerHtmlParserFlat()
        parser.feed(html)
        return parser

