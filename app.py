import os
from hashlib import sha256
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.compat import escape
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from rfc_pretty.datatracker import (
    DatatrackerRfcReader,
    RfcNotFoundError
)

def _generate_etag(rfc_number, length):
    md = sha256()
    md.update(str(rfc_number).encode())
    digest = md.hexdigest()[:20]
    return f"{length}-{rfc_number}-{digest}"

def _set_etag_and_cache_control(request, response):
    rfc_number = _get_rfc_number(request.matchdict['rfc'])
    max_age = os.getenv('RFC_PRETTY_MAX_AGE', '3600')
    if max_age:
        response.cache_control.max_age = int(max_age)
    response.etag = _generate_etag(rfc_number, response.content_length)

def _get_rfc_number(inputstr):
    try:
        return int(inputstr)
    except ValueError:
        if inputstr.upper().startswith('RFC'):
            return int(inputstr[3:])
        raise

def rfc_renderer(request):
    try:
        rfc_number = _get_rfc_number(request.matchdict['rfc'])

        # Check if cache is stale, if asked for.
        if request.if_none_match:
            etag = request.if_none_match.etags[0]
            length = etag.split("-", maxsplit=1)[0]
            etag = _generate_etag(rfc_number, length)
            if etag in request.if_none_match.etags:
                return Response(status_code=304, content_length=length)

        # Generate a response.
        # First, set a response callback that will set Cache-Control and ETag headers,
        # Then, generate the actual response.
        request.add_response_callback(_set_etag_and_cache_control)

        datatracker_url = 'https://datatracker.ietf.org/doc/rfc%d/' % rfc_number
        reader = DatatrackerRfcReader()
        rfc = reader.read(rfc_number)
        return {
            'rfc_number': rfc_number,
            'rfc_title': rfc.title,
            'datatracker_url': datatracker_url,
            'abstract': rfc.abstract,
            'sections': rfc.sections,
            'rfc': rfc
        }
    except (ValueError, RfcNotFoundError):
        return Response(body='Invalid RFC number', content_type='text/plain', status_code=400)

if __name__ == '__main__':
    with Configurator() as config:
        config.add_route('rfc_render', '/RFC{rfc}')
        config.include('pyramid_jinja2')
        config.add_static_view(name='static', path='static')
        config.add_view(rfc_renderer, route_name='rfc_render', renderer='index.jinja2')
        app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 6543, app)
    server.serve_forever()
